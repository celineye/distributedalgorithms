package com.lightbend.akka.sample.projet;

import akka.actor.ActorRef;

import java.util.ArrayList;

public class ListOfActors
{
    public ArrayList<ActorRef> l;

    public ListOfActors(ArrayList<ActorRef> l) {
        this.l = org.apache.commons.lang3.SerializationUtils.clone(l);
    }
}
