package com.lightbend.akka.sample.projet;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Céline YE and Florian ERNST
 * @description Send a message "l" to n actors, containing a list of references of all actors.
 * Upon reception of "l", every actor must sends a "welcome" message to all actors in "l".
 */
public class Main
{

    public static void main(String[] args)
    {
        // Instantiate an actor system
        final ActorSystem system = ActorSystem.create("system");

        final int n = 3;
        List<ActorRef> l = new ArrayList<>();

        // Start by creating array of actors
        for (int i = 0; i < n; i++) {
            String name = "a" + (i + 1);
            l.add(system.actorOf(Process.createActor(), name));
        }

        // Send "l" to every actor
        for (ActorRef actor : l) {
            actor.tell(l, ActorRef.noSender());
        }

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException
    {
        Thread.sleep(5000);
    }
}