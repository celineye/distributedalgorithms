package com.lightbend.akka.sample.projet;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Process extends UntypedAbstractActor
{
    // Logger attached to actor
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    // List of all actors
    private List<ActorRef> l;

    // Keys
    // private Map<Integer, Integer> map;

    // Empty Constructor
    public Process()
    {
        // map = new HashMap<>();
    }

    // Static function that creates actor
    static Props createActor()
    {
        return Props.create(Process.class, () -> {
            return new Process();
        });
    }

/*
    void put(Integer key, Integer value)
    {
        map.put(key, value);
    }

    Integer get(Integer key)
    {
        return map.get(key);
    }
*/

    @Override
    public void onReceive(Object message) throws Throwable
    {
        if (message instanceof ListOfActors) {
            ListOfActors l = (ListOfActors) message;
            for (ActorRef actorRef : l.l) {
                actorRef.tell(new WelcomeMessage("Welcome"), getSelf());
            }
        }
        else if (message instanceof WelcomeMessage) {
            log.info("[" + getSelf().path().name() + "] received message from [" + getSender().path().name() + "] with data: [" + message + "].");
        }
    }
}